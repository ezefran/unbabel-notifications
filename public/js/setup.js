function setup() {
  setupNotification();
}

function setupNotification() {
  chrome.notifications.onClicked.addListener((notificationId) => {
    if (notificationId == NOTIFICATION_ID) {
      chrome.tabs.create({ url: URL_DO_TASK });
    }
  });
}
