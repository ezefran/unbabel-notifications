function main() {
  try {
    setup();
    queryUnbabelData();
    setTimeout(loop, TIME_BETWEEN_CALLS);
  } catch (error) {
    console.error(error.message);
  }
}

main();
