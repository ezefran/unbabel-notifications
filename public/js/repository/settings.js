const initialSettings = {
  minTasks: 1,
  timeFrom: "16:30",
  timeTo: "20:00",
};

const loadSettings = async () => {
  const settings = await getSettingsFromStorage();

  if (chrome.runtime.lastError) {
    console.error("Unexpected error while trying to retrieve settings.");
  } else {
    if (settings) {
      return settings;
    } else {
      console.warn("No saved settings found.");
      return initialSettings;
    }
  }
};

const getSettingsFromStorage = () => {
  return new Promise((resolve, reject) => {
    chrome.storage.local.get(["settings"], function (data) {
      resolve(data.settings);
    });
  });
};
