const queryUnbabelData = async () => {
  try {
    let result = {};
    let responseData;

    const response = await fetch(URL_AVAILABLE_TASKS, {
      method: "GET",
      mode: "no-cors",
    });
    if (response.status === 401) {
      result.error = "You must log in first to receive notifications.";
      state.connected = false;
    } else if (response.status === 0 || response.status !== 200) {
      result.error = "Unexpected error.";
      state.connected = false;
    } else {
      responseData = await response.json();
      if (responseData.paid == null) {
        result.error = "Error while trying to fetch data.";
        state.connected = false;
      }
    }

    if (!result.error) {
      state.errorNotified = false;
      state.connected = true;
      result = {
        pay: round(responseData.paid[0].language_pair.hourly_rate),
        tasks: responseData.paid[0].tasks_available,
        balance: await findBalance(),
      };
      chrome.storage.local.set({ info_unbabel: result });
    }

    state.error = result.error;
    chrome.storage.local.set({ backgroundState: state });

    return result;
  } catch (error) {
    console.error(error.message);
  }
};

const findBalance = async () => {
  const response = await fetch(URL_DASHBOARD);
  const responseHtml = await response.text();
  let doc = new DOMParser().parseFromString(responseHtml, "text/html");

  let script = null;
  let scripts = doc.getElementsByTagName("script");
  for (let i = 0; i < scripts.length; ++i) {
    if (
      scripts[i].childNodes.length > 0 &&
      scripts[i].childNodes[0].nodeValue.includes(
        "window.VUE_APP_EDITOR_DASHBOARD_DJANGO_CONTEXT"
      )
    ) {
      script = scripts[i];
    }
  }

  let regex = /{.*/g;
  return parseFloat(
    JSON.parse(regex.exec(script.innerText)).user.balance.replace("$", "")
  );
};

const round = (number) => {
  return Math.round(number + Number.EPSILON) / 100;
};
