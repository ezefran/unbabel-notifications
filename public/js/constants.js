const TIME_BETWEEN_CALLS = 15 * 1000;
const TIME_BETWEEN_CALLS_DISCONNECTED = 30 * 1000;
const TIME_BETWEEN_NOTIFICATIONS = 3 * 60 * 1000;
const TIMEOUT_AFTER_NOTIFICATION = TIME_BETWEEN_NOTIFICATIONS - 3 * 1000;
const NOTIFICATION_ID = "ff_unbabel_notification_id";
const URL_AVAILABLE_TASKS = "https://unbabel.com/api/v1/available_tasks/";
const URL_DASHBOARD = "https://unbabel.com/editor/dashboard/";
const URL_DO_TASK = "https://unbabel.com/editor/polyglot/landing";
