async function loop() {
  try {
    const settings = await loadSettings();

    const { timeFrom, timeTo } = settings;
    if (!shouldQueryData(timeFrom, timeTo)) return;

    let result = await queryUnbabelData();
    if (result.error && !state.errorNotified) {
      state.errorNotified = true;
      showNotification(result.error);
    } else if (result.tasks >= 0) {
      chrome.tabs.query({ active: true, currentWindow: true }, async (tabs) => {
        const isLessThanMinTasks = result.tasks < settings.minTasks;
        const tabTitle = tabs[0] == null ? "" : tabs[0].title;
        const isViewingTasksPage = tabTitle == "Unbabel - Paid Tasks";

        if (isLessThanMinTasks || isViewingTasksPage) return;

        if (!state.tasksNotified) {
          state.tasksNotified = true;
          showNotification("There are " + result.tasks + " tasks available.");
          setTimeout(
            () => (state.tasksNotified = false),
            TIMEOUT_AFTER_NOTIFICATION
          );
        }
      });
    }
  } catch (error) {
    console.error(error.message);
  }

  let timeout;
  if (state.connected) {
    if (state.tasksNotified) {
      timeout = TIME_BETWEEN_NOTIFICATIONS;
    } else {
      timeout = TIME_BETWEEN_CALLS;
    }
  } else {
    timeout = TIME_BETWEEN_CALLS_DISCONNECTED;
  }
  setTimeout(loop, timeout);
}

function showNotification(message) {
  chrome.notifications.clear(NOTIFICATION_ID, () => {});
  chrome.notifications.create(
    NOTIFICATION_ID,
    {
      iconUrl: "./icon.png",
      type: chrome.notifications.TemplateType.BASIC,
      title: "Unbabel Notifications",
      message: message,
    },
    () => {}
  );
}

function shouldQueryData(timeFrom, timeTo) {
  const now = dayjs().format("HH:mm");
  const betweenTimes = now >= timeFrom && now < timeTo;
  const betweenTimesFragmented = now >= timeFrom || now < timeTo;
  return (
    (timeFrom < timeTo && betweenTimes) ||
    (timeFrom > timeTo && betweenTimesFragmented) ||
    timeFrom === timeTo
  );
}
