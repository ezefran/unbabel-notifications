import React from "react";
import { Link } from "react-router-dom";
import styles from "./css/Icon.module.css";

function Icon(props) {
  const { name, page, style } = props;

  return (
    <Link to={"/" + page} style={style}>
      <i className={`${styles.Icon} fas fa-${name}`}></i>
    </Link>
  );
}

export default Icon;
