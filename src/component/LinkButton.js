import React from "react";
import styles from "./css/LinkButton.module.css";

function LinkButton(props) {
  const { text, url } = props;

  return (
    <a
      href={url}
      target="_blank"
      rel="noopener noreferrer"
      className={styles.LinkButton}
    >
      {text}
    </a>
  );
}

export default LinkButton;
