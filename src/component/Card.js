import React from "react";
import styles from "./css/Card.module.css";

function Card(props) {
  const isMoney = props.type === "money";

  const color = isMoney ? "green" : props.type === "number" ? "blue" : "blue";

  const colorStyle = "var(--color-" + color + ")";

  return (
    <div className={styles.Card}>
      <p className={styles.description}>{props.description}</p>
      <p className={styles.value} style={{ color: colorStyle }}>
        {(isMoney ? "$" : "") + props.value}
      </p>
    </div>
  );
}

export default Card;
