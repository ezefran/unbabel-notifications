import React from "react";
import styles from "./css/SettingsPage.module.css";

import LinkButton from "../component/LinkButton";
import Icon from "../component/Icon";

const URL_DASHBOARD = "https://unbabel.com/editor/dashboard/";
const URL_TASKS = "https://unbabel.com/editor/polyglot/landing";

function SettingsPage(props) {
  const { minTasks, timeFrom, timeTo } = props.settings;
  const { updateSettings } = props;

  return (
    <div className={styles.SettingsPage}>
      <section className={styles.configs}>
        <p style={{ marginBottom: "45px" }}>
          Send notification when&nbsp;
          <input
            type="text"
            value={minTasks}
            maxLength="3"
            onChange={(e) => updateSettings({ minTasks: e.target.value })}
          />
          &nbsp; or more tasks are available.
        </p>
        <p>
          Only show notifications from &nbsp;
          <input
            type="time"
            value={timeFrom}
            onChange={(e) => updateSettings({ timeFrom: e.target.value })}
          />
          &nbsp;to&nbsp;
          <input
            type="time"
            value={timeTo}
            onChange={(e) => updateSettings({ timeTo: e.target.value })}
          />
          &nbsp;.
        </p>
      </section>
      <section className={styles.buttons}>
        <Icon name="arrow-left" page="board" style={{ marginRight: "auto" }} />
        <LinkButton url={URL_DASHBOARD} text="Dashboard" />
        <LinkButton url={URL_TASKS} text="Tasks" />
      </section>
    </div>
  );
}

export default SettingsPage;
