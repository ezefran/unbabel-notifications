import React from "react";
import styles from "./css/BoardPage.module.css";

import Card from "../component/Card";
import LinkButton from "../component/LinkButton";
import Icon from "../component/Icon";

const URL_DASHBOARD = "https://unbabel.com/editor/dashboard/";
const URL_TASKS = "https://unbabel.com/editor/polyglot/landing";

function BoardPage(props) {
  const { unbabelData, backgroundState } = props;

  return (
    <div className={styles.BoardPage}>
      <section className={styles.cards}>
        <Card description="BALANCE" value={unbabelData.balance} type="money" />
        <Card description="TASKS" value={unbabelData.tasks} type="number" />
        <Card description="PAY/Hour" value={unbabelData.pay} type="money" />
      </section>
      <div className={styles.error}>
        {!backgroundState.connected ? <p>{backgroundState.error}</p> : null}
      </div>
      <section className={styles.buttons}>
        <Icon name="cog" page="settings" style={{ marginRight: "auto" }} />
        <LinkButton url={URL_DASHBOARD} text="Dashboard" />
        <LinkButton url={URL_TASKS} text="Tasks" />
      </section>
    </div>
  );
}

export default BoardPage;
