let backgroundState = {
  connected: true,
  error: "You must log in to receive notifications",
};

export const loadBackgroundState = () => {
  return backgroundState;
};
