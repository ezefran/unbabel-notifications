/*global chrome*/

const initialBackgroundState = {
  connected: false,
  error: "You must log in first to receive notifications.",
};

export const loadBackgroundState = async () => {
  const backgroundState = await getBackgroundStateFromStorage();

  if (chrome.runtime.lastError) {
    console.error("Unexpected error while trying to retrieve backgroundState.");
  } else {
    if (backgroundState) {
      return backgroundState;
    } else {
      console.warn("No saved backgroundState found.");
      return initialBackgroundState;
    }
  }
};

const getBackgroundStateFromStorage = () => {
  return new Promise((resolve, reject) => {
    chrome.storage.local.get(["backgroundState"], function (data) {
      resolve(data.backgroundState);
    });
  });
};
