let settings = {
  minTasks: 1,
  timeFrom: "16:30",
  timeTo: "20:00",
};

export const saveSettings = (updatedSettings) => {
  settings = updatedSettings;
};

export const loadSettings = () => {
  return settings;
};
