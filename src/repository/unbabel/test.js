export const TIME_BETWEEN_CALLS = 1000;

export const queryUnbabelData = () => {
  const query = {
    balance: Math.floor(Math.random() * 10),
    pay: Math.floor(Math.random() * 10),
    tasks: Math.floor(Math.random() * 10),
  };

  return query;
};
