/*global chrome*/

const initialData = {
  balance: "-",
  pay: "-",
  tasks: "-",
};

export const TIME_BETWEEN_CALLS = 10000;

export const queryUnbabelData = async () => {
  const result = await getUnbabelDataFromStorage();

  if (chrome.runtime.lastError) {
    console.error("Unexpected error while trying to retrieve unbabel data.");
  } else {
    if (result) {
      return {
        balance: String(result.balance),
        pay: String(result.pay),
        tasks: String(result.tasks),
      };
    } else {
      console.warn("No saved unbabel data found.");
      return initialData;
    }
  }
};

const getUnbabelDataFromStorage = () => {
  return new Promise((resolve, reject) => {
    chrome.storage.local.get(["info_unbabel"], function (data) {
      resolve(data.info_unbabel);
    });
  });
};
