let repos = {};

if (process.env.NODE_ENV === "production") {
  repos = {
    BACKGROUND_STATE: require("./background_state/background_state"),
    UNBABEL: require("./unbabel/unbabel"),
    SETTINGS: require("./settings/settings"),
  };
} else {
  repos = {
    BACKGROUND_STATE: require("./background_state/test"),
    UNBABEL: require("./unbabel/test"),
    SETTINGS: require("./settings/test"),
  };
}

export default repos;
