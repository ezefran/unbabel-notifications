import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import styles from "./css/App.module.css";

import BoardPage from "./pages/BoardPage";
import SettingsPage from "./pages/SettingsPage";

import repos from "./repository/repos";

const { loadBackgroundState } = repos.BACKGROUND_STATE;
const { queryUnbabelData, TIME_BETWEEN_CALLS } = repos.UNBABEL;
const { saveSettings, loadSettings } = repos.SETTINGS;

function App() {
  const [settings, setSettings] = useState({});
  const [unbabelData, setUnbabelData] = useState({
    balance: "-",
    pay: "-",
    tasks: "-",
  });
  const [backgroundState, setBackgroundState] = useState({});

  const updateSettings = (setting) => setSettings({ ...settings, ...setting });

  useEffect(() => {
    const query = async () => {
      try {
        const loadedSettings = await loadSettings();
        setSettings(loadedSettings);
      } catch (error) {
        console.error(error.message);
      }
    };

    query();
  }, []);

  useEffect(() => {
    saveSettings(settings);
  }, [settings]);

  useEffect(() => {
    const query = async () => {
      try {
        const loadedBackgroundState = await loadBackgroundState();
        setBackgroundState(loadedBackgroundState);
      } catch (error) {
        console.error(error.message);
      }
    };

    query();
  }, []);

  useEffect(() => {
    const query = async () => {
      try {
        const unbabelDataQuery = await queryUnbabelData();
        setUnbabelData(unbabelDataQuery);
      } catch (error) {
        console.error(error.message);
      }
    };

    query();
    const interval = setInterval(query, TIME_BETWEEN_CALLS);

    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <div className={styles.App}>
      <Router>
        <Switch>
          <Route
            path="/settings"
            render={() => (
              <SettingsPage
                settings={settings}
                updateSettings={updateSettings}
              />
            )}
          />
          <Route
            path={["/board", "/"]}
            render={() => (
              <BoardPage
                unbabelData={unbabelData}
                backgroundState={backgroundState}
              />
            )}
          />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
